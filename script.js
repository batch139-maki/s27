const http = require("http")

let port = 4000

const server = http.createServer((request, response) => {

	if(request.url == '/'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.write("Welcome to Booking System!")
		response.end()
	}else if(request.url == '/profile'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.write("Welcome to your profile")
		response.end()
	}else if(request.url == '/courses'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.write("Here's our courses available")
		response.end()
	}else if(request.url == '/addcourse'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.write("Add a course to our resources")
		response.end()
	}else if(request.url == '/updatecourse'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.write("Update a course to our resources")
		response.end()
	}else if(request.url == '/archivecourses'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.write("Archive courses to our resources")
		response.end()
	}
});

server.listen(port);

console.log(`Server now accessible at localhost:${port}`)